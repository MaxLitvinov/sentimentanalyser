package com.teadust.sentimentanalyser.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.teadust.sentimentanalyser.R;


public class TextFragment extends Fragment implements TextFragmentView {

    private TextView mText;
    private ProgressBar mProgressBar;

    public static TextFragment newInstance() {
        TextFragment textFragment = new TextFragment();
        return textFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_text, container, false);
        mText = (TextView) rootView.findViewById(R.id.text);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progress);
        return rootView;
    }

    @Override
    public TextView getTextView() {
        return mText;
    }

    @Override
    public ProgressBar getProgressBar() {
        return mProgressBar;
    }
}
