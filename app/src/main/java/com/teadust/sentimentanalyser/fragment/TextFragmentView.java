package com.teadust.sentimentanalyser.fragment;

import android.widget.ProgressBar;
import android.widget.TextView;

public interface TextFragmentView {

    TextView getTextView();
    ProgressBar getProgressBar();
}
