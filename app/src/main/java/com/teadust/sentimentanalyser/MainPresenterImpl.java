package com.teadust.sentimentanalyser;

import android.widget.ProgressBar;
import android.widget.TextView;

import com.teadust.sentimentanalyser.utils.TextLoader;

import java.io.File;

public class MainPresenterImpl implements MainPresenter {

    @Override
    public void loadText(TextView destinationView, ProgressBar progressBar, File selectedFile) {
        new TextLoader(destinationView, progressBar).execute(selectedFile);
    }
}
