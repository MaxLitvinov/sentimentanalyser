package com.teadust.sentimentanalyser;

public interface MainView {

    void initUI();
    void initLogic();

    // Below are methods that belong to NavigationView menu.
    void onOpenFileDialogClick();
    void onSettingsClick();
}
