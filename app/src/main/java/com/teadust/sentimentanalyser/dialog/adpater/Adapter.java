package com.teadust.sentimentanalyser.dialog.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.teadust.sentimentanalyser.R;

import java.io.File;

public class Adapter extends BaseAdapter {

    private Context mContext;
    private File[] mPaths;

    private AdapterListener mAdapterListener;

    public interface AdapterListener {
        void fileClicked(String fileName);
    }

    public Adapter(Context context, FileExplorer fileExplorer) {
        mContext = context;
        mPaths = fileExplorer.getPaths();
    }

    public Adapter setListener(AdapterListener listener) {
        mAdapterListener = listener;
        return this;
    }

    @Override
    public int getCount() {
        return mPaths.length;
    }

    @Override
    public File getItem(int position) {
        return mPaths[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        PathHolder pathHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.dialog_adapter, parent, false);
            pathHolder = new PathHolder();
            pathHolder.mIcon = (ImageView) convertView.findViewById(R.id.icon);
            pathHolder.mPath = (TextView) convertView.findViewById(R.id.path);
            pathHolder.mParent = (LinearLayout) convertView.findViewById(R.id.ll_path);
            pathHolder.mParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View parentLinearLayout) {
                    onPathClick(parentLinearLayout);
                }
            });
            convertView.setTag(pathHolder);
        } else {
            pathHolder = (PathHolder) convertView.getTag();
        }

        setIconAndName(mPaths[position], pathHolder);

        return convertView;
    }

    private void onPathClick(View parent) {
        String pathName = ((PathHolder) parent.getTag()).mPath.getText().toString();
        mAdapterListener.fileClicked(pathName);
    }

    private void setIconAndName(File path, PathHolder pathHolder) {
        if (path.isDirectory()) {
            pathHolder.mIcon.setImageResource(R.drawable.folder);
        } else {
            pathHolder.mIcon.setImageResource(R.drawable.file);
        }
        pathHolder.mPath.setText(path.getName());
    }

    public class PathHolder {

        LinearLayout mParent;
        ImageView mIcon;
        TextView mPath;
    }
}
