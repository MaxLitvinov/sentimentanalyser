package com.teadust.sentimentanalyser.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;

import com.teadust.sentimentanalyser.R;
import com.teadust.sentimentanalyser.dialog.adpater.Adapter;
import com.teadust.sentimentanalyser.dialog.adpater.FileExplorer;

import java.io.File;


public class OpenFileDialog extends Dialog implements OpenFileDialogView {

    private static final String PARENT_DIR = "..";

    private Context mContext;
    private TextView mTitle;
    private ListView mPathList;

    private File mCurrentPath;
    private FileSelectedListener mFileSelectedListener;

    public interface FileSelectedListener {
        void fileSelected(File selectedFile);
    }

    public OpenFileDialog(@NonNull Context context) {
        super(context);
        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog);

        initUI();
    }

    @Override
    public OpenFileDialog setFileListener(FileSelectedListener listener) {
        mFileSelectedListener = listener;
        return this;
    }

    @Override
    public void initUI() {
        mTitle = (TextView) findViewById(R.id.title);
        mPathList = (ListView) findViewById(R.id.path_list);
        refreshDialog(Environment.getExternalStorageDirectory());
    }

    @Override
    public void onItemClick(String fileName) {
        File selectedFile = getSelectedFile(fileName);
        if (selectedFile.isDirectory()) {
            refreshDialog(selectedFile);
        } else {
            if (mFileSelectedListener != null) {
                mFileSelectedListener.fileSelected(selectedFile);
            }
            this.dismiss();
        }
    }

    private File getSelectedFile(String fileName) {
        if (fileName.equals(PARENT_DIR)) {
            return mCurrentPath.getParentFile();
        }
        return new File(mCurrentPath, fileName);
    }

    private void refreshDialog(final File path) {
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mCurrentPath = path;
        if (path.exists()) {
            mTitle.setText(mCurrentPath.getPath());
            mPathList.setAdapter(
                    new Adapter(mContext, new FileExplorer(path))
                            .setListener(new Adapter.AdapterListener() {
                                @Override
                                public void fileClicked(String fileName) {
                                    onItemClick(fileName);
                                }
                            }));
        }
    }
}
