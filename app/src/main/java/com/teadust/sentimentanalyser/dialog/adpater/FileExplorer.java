package com.teadust.sentimentanalyser.dialog.adpater;

import com.teadust.sentimentanalyser.utils.FileUtils;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;

public class FileExplorer {

    private File[] mPaths;

    public FileExplorer(File currentPath) {
        File[] directories = createDirs(currentPath);
        File[] files = createFiles(currentPath);

        if (currentPath.getParentFile() != null) {
            File[] parentDir = {new File("..")};
            mPaths = concatenateArrays(parentDir, directories, files);
        } else {
            mPaths = concatenateArrays(directories, files);
        }
    }

    public File[] getPaths() {
        return mPaths;
    }

    private File[] createDirs(File sourcePath) {
        File[] paths =  sourcePath.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return (pathname.isDirectory() && pathname.canRead());
            }
        });
        Arrays.sort(paths);
        return paths;
    }

    private File[] createFiles(final File sourcePath) {
        File[] files = sourcePath.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                if (!pathname.isDirectory() && pathname.canRead()) {
                    if (FileUtils.hasRightExtension(pathname.getName())) {
                        return true;
                    }
                }
                return false;
            }
        });
        Arrays.sort(files);
        return files;
    }

    private File[] concatenateArrays(File[]... arrays) {
        int length = 0;
        for (File[] array : arrays) {
            length += array.length;
        }

        final File[] result = new File[length];

        int offset = 0;
        for (File[] array : arrays) {
            System.arraycopy(array, 0, result, offset, array.length);
            offset += array.length;
        }

        return result;
    }
}
