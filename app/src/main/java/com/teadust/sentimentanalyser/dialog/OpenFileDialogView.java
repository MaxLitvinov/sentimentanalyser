package com.teadust.sentimentanalyser.dialog;

public interface OpenFileDialogView {

    OpenFileDialog setFileListener(OpenFileDialog.FileSelectedListener listener);
    void initUI();
    void onItemClick(String fileName);
}
