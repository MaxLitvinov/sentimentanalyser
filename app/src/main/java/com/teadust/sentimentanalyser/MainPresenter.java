package com.teadust.sentimentanalyser;

import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.File;

public interface MainPresenter {

    void loadText(TextView destinationView, ProgressBar progressBar, File selectedFile);
}
