package com.teadust.sentimentanalyser;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.teadust.sentimentanalyser.dialog.OpenFileDialog;
import com.teadust.sentimentanalyser.fragment.TextFragment;

import java.io.File;

public class MainActivity extends AppCompatActivity implements MainView, NavigationView.OnNavigationItemSelectedListener {

    private Toolbar mToolbar;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private NavigationView mNavigationView;

    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;
    private Fragment mCurrentFragment;

    private MainPresenter mMainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUI();
        initLogic();
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void initUI() {
        initToolbar();
        initNavigationView();
        initFragment();
    }

    @Override
    public void initLogic() {
        mMainPresenter = new MainPresenterImpl();
    }

    @Override
    public void onOpenFileDialogClick() {
        new OpenFileDialog(this).setFileListener(new OpenFileDialog.FileSelectedListener() {
            @Override
            public void fileSelected(File selectedFile) {
                mMainPresenter.loadText(
                        ((TextFragment) mCurrentFragment).getTextView(),
                        ((TextFragment) mCurrentFragment).getProgressBar(),
                        selectedFile);
            }
        }).show();
    }

    @Override
    public void onSettingsClick() {
        // TODO: application settings.
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.navigation_menu_open_file) {
            onOpenFileDialogClick();
        } else if (id == R.id.navigation_view_menu_settings) {
            onSettingsClick();
        }

        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
    }

    private void initNavigationView() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar,
                R.string.navigation_view_drawer_open, R.string.navigation_view_drawer_close);
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
        mNavigationView.setItemIconTintList(null);
        mNavigationView.setNavigationItemSelectedListener(this);
    }

    private void initFragment() {
        mCurrentFragment = TextFragment.newInstance();
        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.add(R.id.fragment_container, mCurrentFragment, "TextFragment");
        mFragmentTransaction.commit();
    }
}
