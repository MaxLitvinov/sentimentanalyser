package com.teadust.sentimentanalyser.utils;

public class FileUtils {

    public static boolean hasRightExtension(String fileName) {
        return Validator.hasRightExtension(fileName);
    }

    private static class Validator {

        private static final String[] POSSIBLE_EXTENSIONS = {"txt"};

        static boolean hasRightExtension(String fileName) {
            for (String extension : POSSIBLE_EXTENSIONS) {
                if (fileName.toLowerCase().endsWith(extension)) {
                    return true;
                }
            }
            return false;
        }
    }
}
