package com.teadust.sentimentanalyser.utils;

import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class TextLoader extends AsyncTask<File, String, Void> {

    private TextView mText;
    private ProgressBar mProgress;

    public TextLoader(TextView textView, ProgressBar progressBar) {
        mText = textView;
        mProgress = progressBar;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mProgress.setVisibility(View.VISIBLE);
    }

    @Override
    protected Void doInBackground(File... params) {
        File selectedFile = params[0];
        String filePath = selectedFile.getAbsolutePath();
        try {
            readFile(filePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onProgressUpdate(String... readedFile) {
        mText.append(readedFile[0]);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        mProgress.setVisibility(View.GONE);
    }

    private void readFile(String filePath) throws FileNotFoundException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath));
        try {
            String currentLine;
            while ((currentLine = bufferedReader.readLine()) != null) {
                publishProgress(currentLine);
                publishProgress("\n");
            }
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
